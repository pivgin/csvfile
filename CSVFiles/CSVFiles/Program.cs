﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CSVFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            const string fileName = @"list.csv";
            string inputText = null;
            do
            {
                try
                {
                    if (!File.Exists(fileName)) 
                    {
                        throw new FileNotFoundException();
                    }
                    Console.WriteLine("Programm has started");
                    //StreamReader myReader = new StreamReader(filename);

                    string[] allLinesOfFile = File.ReadAllLines(fileName);

                    foreach (var item in allLinesOfFile)
                    {
                        //Console.WriteLine(item);
                        string[] result = item.Split(',');
                        foreach (var s in result)
                        {
                            Console.WriteLine(s);
                        }
                    }

                }
                catch (FileNotFoundException ex)
                {
                    Console.WriteLine("Could not find the file!" + ex.Message);
                }
                Console.WriteLine("Input N to exit");
                inputText = Console.ReadLine().ToUpper();
            }
            while (inputText != "N");
        }
    }
}
